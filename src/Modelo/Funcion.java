/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Modelo.Punto;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase que representa una colección de puntos de una función
 * @author madar
 */
public class Funcion implements Comparable{
    
    private Punto puntos[];

    public Funcion() {
    }
    
    public Funcion(String datos) {
        String datosFila[]=datos.split(";");
        // Crear el arreglo de puntos
        this.puntos=new Punto[datosFila.length];
        for(int j=0;j<this.puntos.length;j++)
        {
            try {
                this.puntos[j]=new Punto(datosFila[j]);
            } catch (Exception ex) {
                System.out.println("No se puede crear el punto:"+ex.getMessage());
            }
        }
        
        this.ordenarPuntos();
        
    }

    @Override
    public String toString() {
        String msg="";
        for(Punto myPunto:this.puntos)
            msg+=myPunto.toString()+"\t";

        return msg;
    }
    
    /**
     * Ordena el vector de puntos usando el método de la burbuja
     * ver más información: https://runestone.academy/runestone/static/pythoned/SortSearch/ElOrdenamientoBurbuja.html
     * Ordena ascendentemente por el eje X (tomar en cuenta todas las normas de nuestra matemáticas)
     */
    
    private void ordenarPuntos()
    {
            // :)
    }
    
    

    public Punto[] getPuntos() {
        return puntos;
    }

    public void setPuntos(Punto[] puntos) {
        this.puntos = puntos;
    }

    @Override
    public int compareTo(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
