/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Punto;
import javax.swing.JOptionPane;

/**
 *
 * http://www.aprendeaprogramar.com/mod/resource/view.php?id=462
 * @author madarme
 */
public class PruebaPunto_Caputura_Opcional_try {
    public static void main(String[] args) {
        
        String datoX=JOptionPane.showInputDialog(null, "Digite punto x","Punto x", JOptionPane.QUESTION_MESSAGE);
        String datoY=JOptionPane.showInputDialog(null, "Digite punto x","Punto y", JOptionPane.QUESTION_MESSAGE);
        try{
        Punto p1=new Punto(datoX, datoY);
        JOptionPane.showMessageDialog(null, "El punto fue:"+p1.toString());
        }catch(RuntimeException e) 
        {
        JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }
    
}
